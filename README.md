# Employee_Info


This is a test app for Appiness Interactive Pvt. Ltd.

**Steps to installtion**
- Download .zip of application and unzip.
- Give the file path on Terminal(MAC) and CMD(windows)
- Install dependencies: **npm install --save**

**iOS**
- Give project path on terminal and run command **react-native run-ios**
- App will excute on simulator
- If having any error delete derived data from **~/Library/Developer/Xcode/DerivedData**
- Delete build folder from ios folder


**Android**
- Connect any android device. Make sure enable the USB Debugging of device.
- Give project path on terminal and run command **react-native run-android**
- App will excute on device