package com.userinfo_demo;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import java.util.Timer;
import java.util.TimerTask;


/**
 * Created by kshitij on 09/03/18.
 */

public class SplashActivity extends AppCompatActivity {


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = new Intent(SplashActivity.this, MainActivity.class);
        startActivity(intent);
        Timer t = new Timer();
        t.schedule(new TimerTask() {

            @Override
            public void run() {
                finish();
                // If you want to call Activity then call from here for 5 seconds it automatically call and your image disappear....
            }
        }, 5000);

    }
}
